<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\PurchashingBundle\Tests;

use Kematjaya\PurchashingBundle\Entity\PurchaseInterface;
use Kematjaya\PurchashingBundle\Entity\PurchaseDetailInterface;
use Kematjaya\PurchashingBundle\Repo\PurchaseRepoInterface;
use Kematjaya\PurchashingBundle\Manager\PurchasingManager;
use Kematjaya\ItemPackBundle\Service\StockServiceInterface;
use Kematjaya\ItemPackBundle\Manager\StockManagerInterface;
use Kematjaya\ItemPackBundle\Entity\ItemInterface;
use Kematjaya\ItemPackBundle\Entity\ItemPackageInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use PHPUnit\Framework\TestCase;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Description of PurchasingManagerTest
 *
 * @author programmer
 */
class PurchasingManagerTest extends TestCase 
{
    public function testLocked()
    {
        $item = $this->createMock(ItemInterface::class);
        $itemPackage = $this->createMock(ItemPackageInterface::class);
        $itemPackage->expects($this->any())
                ->method("isSmallestUnit")
                ->willReturn(true);
        
        $item->expects($this->any())
                ->method("getItemPackages")
                ->willReturn(new ArrayCollection([
                    $itemPackage
                ]));
        
        $detail = $this->createMock(Model\PurchaseDetailAlsoStockCardInterface::class);
        $detail->expects($this->any())
                ->method("getItem")
                ->willReturn($item);
        $detail->method("getQuantity")
                ->willReturn((float)10);
        $detail->method("getTotal")
                ->willReturn((float)20000);
        $entity = $this->createMock(PurchaseInterface::class);
        $entity->method("getIsLocked")
                ->willReturn(true);
        $entity->method("getPurchaseDetails")
                ->willReturn(
                    new ArrayCollection([$detail])
                );
        
        $stockManager = $this->createMock(StockManagerInterface::class);
        $stockManager->expects($this->once())
                ->method("insertStock")
                ->willReturn($detail->getQuantity());
        
        $purchaseRepository = $this->createMock(PurchaseRepoInterface::class);
        $purchaseRepository->expects($this->once())
                ->method("save");
        
        $eventDispatcher = $this->createMock(EventDispatcherInterface::class);
        $eventDispatcher->expects($this->any())
                ->method("dispatch");
        
        $manager = new PurchasingManager(
                $stockManager, 
                $purchaseRepository,
                $eventDispatcher 
            );
        
        $manager->update($entity);
        
    }
}
