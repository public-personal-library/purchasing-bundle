<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\PurchashingBundle\Tests;

use Kematjaya\ItemPackBundle\Entity\PackagingInterface;
use Kematjaya\ItemPackBundle\Entity\ItemPackageInterface;
use Kematjaya\ItemPackBundle\Entity\ItemInterface;
use Kematjaya\ItemPackBundle\Service\PriceLogServiceInterface;
use Kematjaya\ItemPackBundle\Service\StockCardServiceInterface;
use Kematjaya\PurchashingBundle\Tests\Model\PurchaseDetailAlsoStockCardInterface;
use Kematjaya\PurchashingBundle\EventSubscriber\InsertStockCardEventSubscriber;
use Kematjaya\PurchashingBundle\EventSubscriber\UpdatePrincipalPriceEventSubscriber;
use Kematjaya\PurchashingBundle\Event\PostAddStockEvent;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Description of PostAddStockEventTest
 *
 * @author programmer
 */
class PostAddStockEventTest extends \PHPUnit\Framework\TestCase 
{
    public function testInsertStockCard()
    {
        $stockCardService = $this->createMock(StockCardServiceInterface::class);
        $stockCardService->expects($this->once())
                ->method("insertStockCard");
        
        $purchaseDetail = $this->createMock(PurchaseDetailAlsoStockCardInterface::class);
        $event = $this->createMock(PostAddStockEvent::class);
        $event->expects($this->once())
                ->method("getPurchaseDetail")
                ->willReturn($purchaseDetail);
        
        $subscriber = new InsertStockCardEventSubscriber($stockCardService);
        $subscriber->insertStockCard($event);
    }
    
    public function testUpdatePrincipalPrice()
    {
        $priceLogService = $this->createMock(PriceLogServiceInterface::class);
        $priceLogService->expects($this->once())
                ->method("saveNewPrice");
        
        $subscriber = new UpdatePrincipalPriceEventSubscriber($priceLogService);
        
        $quantity = (float) 10;
        $total = (float) 20000;
        
        $packaging = $this->createMock(PackagingInterface::class);
        $packaging->expects($this->any())
                ->method("getCode")
                ->willReturn("001");
        
        $itemPackage = $this->createMock(ItemPackageInterface::class);
        $itemPackage->expects($this->once())
                ->method("getPackaging")
                ->willReturn($packaging);
        
        $itemPackage->expects($this->once())
                ->method("isSmallestUnit")
                ->willReturn(false);
        
        $itemPackage->expects($this->once())
                ->method("getQuantity")
                ->willReturn(1.0);
        
        $item = $this->createMock(ItemInterface::class);
        $item->expects($this->any())
                ->method("getItemPackages")
                ->willReturn(
                    new ArrayCollection(
                        [$itemPackage]
                    )
                );
        
        $purchaseDetail = $this->createMock(PurchaseDetailAlsoStockCardInterface::class);
        $purchaseDetail->expects($this->any())
                ->method("getQuantity")
                ->willReturn($quantity);
        
        $purchaseDetail->expects($this->any())
                ->method("getPackaging")
                ->willReturn($packaging);
        
        $purchaseDetail->expects($this->any())
                ->method("getTotal")
                ->willReturn($total);
        
        $event = $this->createMock(PostAddStockEvent::class);
        $event->expects($this->once())
                ->method("getPurchaseDetail")
                ->willReturn($purchaseDetail);
        
        $event->expects($this->once())
                ->method("getItem")
                ->willReturn($item);
        
        $subscriber->updatePrincipalPrice($event);
    }
}
