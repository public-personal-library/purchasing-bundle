<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPInterface.php to edit this template
 */

namespace Kematjaya\PurchashingBundle\Tests\Model;

use Kematjaya\ItemPackBundle\Entity\StoreStockCardTransactionInterface;
use Kematjaya\PurchashingBundle\Entity\PurchaseDetailInterface;

/**
 *
 * @author programmer
 */
abstract class PurchaseDetailAlsoStockCardInterface implements PurchaseDetailInterface, StoreStockCardTransactionInterface
{
    //put your code here
}
