<?php

namespace Kematjaya\PurchashingBundle\Entity;

use Kematjaya\PurchashingBundle\Entity\PurchaseInterface;
use Kematjaya\ItemPackBundle\Entity\PackagingInterface;
use Kematjaya\ItemPackBundle\Entity\ItemInterface;
use Kematjaya\ItemPackBundle\Entity\StoreStockCardTransactionInterface;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
interface PurchaseDetailInterface extends StoreStockCardTransactionInterface
{
    public function getItem(): ?ItemInterface;

    public function getQuantity(): ?float;

    public function getPrice(): ?float;

    public function getTotal(): ?float;

    public function getTax(): ?float;

    public function getPurchase(): ?PurchaseInterface;

    public function getPackaging(): ?PackagingInterface;
    
}
