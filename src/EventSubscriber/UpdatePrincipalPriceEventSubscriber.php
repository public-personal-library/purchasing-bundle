<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\PurchashingBundle\EventSubscriber;

use Kematjaya\ItemPackBundle\Entity\ItemPackageInterface;
use Kematjaya\ItemPackBundle\Entity\PackagingInterface;
use Kematjaya\ItemPackBundle\Entity\ItemInterface;
use Kematjaya\ItemPackBundle\Service\PriceLogServiceInterface;
use Kematjaya\PurchashingBundle\Event\PostAddStockEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Description of UountPrincipalPriceEventSubscriber
 *
 * @author programmer
 */
class UpdatePrincipalPriceEventSubscriber implements EventSubscriberInterface
{
    /**
     * 
     * @var PriceLogServiceInterface
     */
    private $priceLogService;
    
    public function __construct(PriceLogServiceInterface $priceLogService) 
    {
        $this->priceLogService = $priceLogService;
    }
    
    public static function getSubscribedEvents():array 
    {
        return [
            PostAddStockEvent::EVENT_NAME => [
                ['updatePrincipalPrice', 500]
            ]
        ];
    }
    
    public function updatePrincipalPrice(PostAddStockEvent $event):void
    {
        $item = $event->getItem();
        $purchaseDetail = $event->getPurchaseDetail();
        $price = $this->countPrincipalPrice(
            $item, 
            (float)$purchaseDetail->getTotal(), 
            (float)$purchaseDetail->getQuantity(), 
            $purchaseDetail->getPackaging()
        );

        $this->priceLogService->saveNewPrice($item, $price);
    }
    
    protected function getItemPackByPackagingOrSmallestUnit(ItemInterface $item, PackagingInterface $packaging = null):ItemPackageInterface
    {
        $itemPackages = $item->getItemPackages();
        if ($itemPackages->isEmpty()) {
            throw new \Exception('item package is empty');
        }
        
        $filtered = $itemPackages->filter(function (ItemPackageInterface $itemPackage) use ($packaging) {
            if ($packaging) {
                
                return $packaging->getCode() === $itemPackage->getPackaging()->getCode();
            }
            
            return $itemPackage->isSmallestUnit();
        });
        
        if ($filtered->isEmpty()) {
            throw new \Exception(
                sprintf("cannot find smallest unit for item %s", $item->getCode())
            );
        }
        
        return $filtered->first();
    }
    
    protected function countPrincipalPrice(ItemInterface $item, float $price, float $quantity, PackagingInterface $packaging = null) : float
    {
        if (0 >= $quantity) {
            throw new \Exception(
                sprintf("quantity cannot less than 1")
            );
        }
        
        $itemPackage = $this->getItemPackByPackagingOrSmallestUnit($item, $packaging);
        if ($itemPackage instanceof ItemPackageInterface) {
            $quantity = ($itemPackage->isSmallestUnit()) ? $quantity : $quantity * $itemPackage->getQuantity();
        }
        
        return $price / $quantity;
    }

}
