<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\PurchashingBundle\EventSubscriber;

use Kematjaya\ItemPackBundle\Entity\ClientStockCardInterface;
use Kematjaya\ItemPackBundle\Service\StockCardServiceInterface;
use Kematjaya\PurchashingBundle\Event\PostAddStockEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Description of InsertStockCardEventSubscriber
 *
 * @author programmer
 */
class InsertStockCardEventSubscriber implements EventSubscriberInterface
{
    
    /**
     * 
     * @var StockCardServiceInterface
     */
    private $stockCardService;
    
    public function __construct(StockCardServiceInterface $stockCardService) 
    {
        $this->stockCardService = $stockCardService;
    }
    
    public static function getSubscribedEvents():array
    {
        return [
            PostAddStockEvent::EVENT_NAME => [
                ['insertStockCard', 1000]
            ]
        ];
    }

    public function insertStockCard(PostAddStockEvent $event):void
    {
        $purchaseDetail = $event->getPurchaseDetail();
        if (!$purchaseDetail instanceof ClientStockCardInterface) {
            return;
        }
        
        $this->stockCardService->insertStockCard(
            $event->getItem(), 
            $purchaseDetail
        );
    }
}
