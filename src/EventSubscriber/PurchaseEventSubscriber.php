<?php

namespace Kematjaya\PurchashingBundle\EventSubscriber;

use Kematjaya\PurchashingBundle\Entity\PurchaseInterface;
use Kematjaya\PurchashingBundle\Manager\PurchasingManagerInterface;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Events;
use Doctrine\Common\EventSubscriber;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class PurchaseEventSubscriber implements EventSubscriber
{

    /**
     *
     * @var PurchasingManagerInterface
     */
    private $purchaseManager;

    public function __construct(PurchasingManagerInterface $purchaseManager)
    {
        $this->purchaseManager = $purchaseManager;
    }

    public function getSubscribedEvents():array
    {
        return [
            Events::onFlush
        ];
    }

    public function onFlush(OnFlushEventArgs $eventArgs):void
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            if (!$entity instanceof PurchaseInterface) {
                continue;
            }

            $this->purchaseManager->update($entity, $uow->getEntityChangeSet($entity));
        }

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if (!$entity instanceof PurchaseInterface) {
                continue;
            }

            $this->purchaseManager->update($entity, $uow->getEntityChangeSet($entity));
        }

        foreach ($uow->getScheduledEntityDeletions() as $entity) {
            if (!$entity instanceof PurchaseInterface) {
                continue;
            }

            $this->purchaseManager->update($entity, $uow->getEntityChangeSet($entity));
        }
    }
}
