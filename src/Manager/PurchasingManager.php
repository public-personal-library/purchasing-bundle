<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\PurchashingBundle\Manager;

use Kematjaya\PurchashingBundle\Event\PostSavePurchasingEvent;
use Kematjaya\PurchashingBundle\Event\PostAddStockEvent;
use Kematjaya\PurchashingBundle\Repo\PurchaseRepoInterface;
use Kematjaya\PurchashingBundle\Entity\PurchaseDetailInterface;
use Kematjaya\PurchashingBundle\Entity\PurchaseInterface;
use Kematjaya\ItemPackBundle\Manager\StockManagerInterface;
use Kematjaya\ItemPackBundle\Entity\StoreStockCardTransactionInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Description of PurchasingManager
 *
 * @author programmer
 */
class PurchasingManager implements PurchasingManagerInterface 
{
    
    private PurchaseRepoInterface $purchaseRepository;
    
    private StockManagerInterface $stockManager;
    
    private EventDispatcherInterface $eventDispatcher;
    
    public function __construct(StockManagerInterface $stockManager, PurchaseRepoInterface $purchaseRepository, EventDispatcherInterface $eventDispatcher) 
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->stockManager = $stockManager;
        $this->purchaseRepository = $purchaseRepository;
    }
    
    public function update(PurchaseInterface $entity, array $changeSets = []): void
    {
        $total = 0;
        if (!isset($changeSets["is_locked"])) {

            return;
        }

        if (!$changeSets["is_locked"][1]) {
            return;
        }
        
        foreach ($entity->getPurchaseDetails() as $purchaseDetail) {
            if (!$purchaseDetail instanceof PurchaseDetailInterface) {
                continue;
            }
            
            if (!$purchaseDetail instanceof StoreStockCardTransactionInterface) {
                continue;
            }
            
            $total += $purchaseDetail->getTotal();
            $this->stockManager->insertStock(
                $purchaseDetail->getItem(), 
                $purchaseDetail
            );
            
            $this->eventDispatcher->dispatch(
                new PostAddStockEvent($purchaseDetail->getItem(), $purchaseDetail),
                PostAddStockEvent::EVENT_NAME
            );
        }
        
        $entity->setTotal($total);
        
        $this->purchaseRepository->save($entity);
        
        $this->eventDispatcher->dispatch(
            new PostSavePurchasingEvent($entity),
            PostSavePurchasingEvent::EVENT_NAME
        );
    }
    
}
