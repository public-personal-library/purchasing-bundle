<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPInterface.php to edit this template
 */

namespace Kematjaya\PurchashingBundle\Manager;

use Kematjaya\PurchashingBundle\Entity\PurchaseInterface;

/**
 *
 * @author programmer
 */
interface PurchasingManagerInterface 
{
    public function update(PurchaseInterface $purchase, array $changeSets = []):void;
}
