<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPInterface.php to edit this template
 */

namespace Kematjaya\PurchashingBundle\FormSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 *
 * @author programmer
 */
interface PurchaseFormSubscriberInterface extends EventSubscriberInterface 
{
    const TAG_NAME = 'purchase.form_event_subscriber';
    
    public function isSupported(string $className):bool;
}
