<?php

namespace Kematjaya\PurchashingBundle\FormSubscriber;

use Kematjaya\ItemPackBundle\Entity\ItemInterface;
use Kematjaya\ItemPackBundle\Entity\ItemPackageInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Kematjaya\PriceBundle\Type\PriceType;

/**
 * @deprecated
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ItemPostEventSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents():array
    {
        return [
            FormEvents::POST_SET_DATA => 'postSetData',
            FormEvents::POST_SUBMIT => 'postSubmit'
        ];
    }
    
    public function postSetData(FormEvent $event):void
    {
        $this->changePackaging($event->getForm()->getParent(), $event->getForm()->getData());
    }
    
    public function postSubmit(FormEvent $event):void
    {
        $this->changePackaging($event->getForm()->getParent(), $event->getForm()->getData());
    }
    
    private function changePackaging(FormInterface $form, ItemInterface $item = null):void
    {
        $price = $form->getData()->getPrice();
        if (null === $price) {
            $price = (null !== $item) ? $item->getPrincipalPrice() : 0;
        }
        
        $form->add('packaging', null, [
            'label' => 'packaging',
            'choices' => $this->getChoices($item), "required" => true,
            'attr' => ['class' => 'form-control']
        ])->add('price', PriceType::class, [
            'label' => 'price',
            "data" => $price,
            "required" => true
        ]);
    }
    
    private function getChoices(ItemInterface $item = null):array
    {
        if (!$item) {
            return [];
        }
        
        $choices = [];
        foreach ($item->getItemPackages() as $itemPackage) {
            if ($itemPackage instanceof ItemPackageInterface) {
                $choices[] = $itemPackage->getPackaging();
            }
        }
        
        return $choices;
    }
}
