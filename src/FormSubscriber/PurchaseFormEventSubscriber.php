<?php

namespace Kematjaya\PurchashingBundle\FormSubscriber;

use Kematjaya\PurchashingBundle\Entity\PurchaseInterface;
use Symfony\Component\PropertyInfo\PropertyInfoExtractorInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class PurchaseFormEventSubscriber implements PurchaseFormSubscriberInterface 
{
    
    /**
     * 
     * @var PropertyInfoExtractorInterface
     */
    private $propertyInfoExtractor;
    
    public function __construct(PropertyInfoExtractorInterface $propertyInfoExtractor) 
    {
        $this->propertyInfoExtractor = $propertyInfoExtractor;
    }
    
    public static function getSubscribedEvents():array
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData'
        ];
    }
    
    public function preSetData(FormEvent $event):void
    {
        $data = $event->getData();
        if (!$data instanceof PurchaseInterface) {
            return;
        }
        
        $form = $event->getForm();
        if (!$data->getIsLocked()) {
            if (!$data->getPurchaseDetails()->isEmpty()) {
                $form->add('is_locked', CheckboxType::class, [
                    "label" => "is_locked_?",
                    "required" => false
                ]);
            }
            return;
        }
        
        foreach ($form->all() as $child) {
            $type = get_class($child->getConfig()->getType()->getInnerType());
            $options = $child->getConfig()->getOptions();
            $options["attr"]["readonly"] = true;
            $form->add($child->getName(), $type, $options);
        }
    }

    public function isSupported(string $className): bool 
    {
        $reflection = new \ReflectionClass($className);
        
        return $reflection->isSubclassOf(PurchaseInterface::class);
    }

}
