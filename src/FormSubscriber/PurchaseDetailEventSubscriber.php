<?php

namespace Kematjaya\PurchashingBundle\FormSubscriber;

use Kematjaya\PurchashingBundle\FormSubscriber\PurchaseFormSubscriberInterface;
use Kematjaya\PurchashingBundle\Entity\PurchaseDetailInterface;
use Kematjaya\PriceBundle\Type\PriceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class PurchaseDetailEventSubscriber implements PurchaseFormSubscriberInterface
{
    public static function getSubscribedEvents():array
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData'
        ];
    }

    public function preSetData(FormEvent $event):void
    {
        $obj = $event->getData();
        if (!$obj instanceof PurchaseDetailInterface) {
            return;
        }

        $form = $event->getForm();
        $form
            ->add('quantity', NumberType::class, [
                'label' => 'quantity',
                'html5' => true,
                'attr' => [
                    "onchange" => 'return updateTotal()',
                    "min" => 1
                ]
            ])
            ->add('price', PriceType::class, [
                'label' => 'purchase_price',
                "required" => true,
                'attr' => [
                    'onchange' => 'return updateTotal()'
                ]
            ])
            ->add('tax', PriceType::class, [
                'label' => 'tax',
                "required" => false,
                'attr' => [
                    'onchange' => 'return updateTotal()'
                ]
            ])
            ->add('total', PriceType::class, [
                'label' => 'total',
                "required" => true,
            ]);
    }

    public function isSupported(string $className): bool
    {
        $reflection = new \ReflectionClass($className);

        return $reflection->isSubclassOf(PurchaseDetailInterface::class);
    }

}
