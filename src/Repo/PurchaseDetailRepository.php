<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\PurchashingBundle\Repo;

use Kematjaya\PurchashingBundle\Entity\PurchaseInterface;
use Kematjaya\PurchashingBundle\Entity\PurchaseDetailInterface;

/**
 * Description of PurchaseDetailRepository
 *
 * @author programmer
 */
class PurchaseDetailRepository implements PurchaseDetailRepoInterface 
{
    //put your code here
    public function createObject(PurchaseInterface $purchase): PurchaseDetailInterface 
    {
        throw new Exception(
            sprintf("please implement interface %s with your class", PurchaseDetailRepoInterface::class)
        );
    }

    public function save(PurchaseDetailInterface $package): void 
    {
        throw new Exception(
            sprintf("please implement interface %s with your class", PurchaseDetailRepoInterface::class)
        );
    }

}
