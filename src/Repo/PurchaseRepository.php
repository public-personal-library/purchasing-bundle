<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\PurchashingBundle\Repo;

use Kematjaya\PurchashingBundle\Entity\PurchaseInterface;

/**
 * Description of PurchaseRepository
 *
 * @author programmer
 */
class PurchaseRepository implements PurchaseRepoInterface 
{
    //put your code here
    public function createObject(): PurchaseInterface 
    {
        throw new Exception(
            sprintf("please implement interface %s with your class", PurchaseRepoInterface::class)
        );
    }

    public function save(PurchaseInterface $package): void 
    {
        throw new Exception(
            sprintf("please implement interface %s with your class", PurchaseRepoInterface::class)
        );
    }

}
