<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\PurchashingBundle\CompilerPass;

use Kematjaya\PurchashingBundle\FormSubscriber\PurchaseFormSubscriberInterface;
use Kematjaya\PurchashingBundle\Builder\FormEventSubscriberBuilderInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Description of MenuParserCompilerPass
 *
 * @author programmer
 */
class FormEventSubscriberCompilerPass implements CompilerPassInterface
{
    //put your code here
    public function process(ContainerBuilder $container) 
    {
        $definition = $container->findDefinition(FormEventSubscriberBuilderInterface::class);
        $taggedServices = $container->findTaggedServiceIds(PurchaseFormSubscriberInterface::TAG_NAME);
        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addFormSubscriber', [new Reference($id)]);
        }
    }
}
