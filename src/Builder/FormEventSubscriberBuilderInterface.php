<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPInterface.php to edit this template
 */

namespace Kematjaya\PurchashingBundle\Builder;

use Kematjaya\PurchashingBundle\FormSubscriber\PurchaseFormSubscriberInterface;
use Doctrine\Common\Collections\Collection;

/**
 *
 * @author programmer
 */
interface FormEventSubscriberBuilderInterface 
{
    public function addFormSubscriber(PurchaseFormSubscriberInterface $element): self;

    public function getFormSubscribers(string $className):Collection;
}
