<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\PurchashingBundle\Builder;

use Kematjaya\PurchashingBundle\FormSubscriber\PurchaseFormSubscriberInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Description of FormEventSubscriberBuilder
 *
 * @author programmer
 */
class FormEventSubscriberBuilder implements FormEventSubscriberBuilderInterface, EventSubscriberInjectorInterface 
{
    /**
     * 
     * @var Collection
     */
    private $elements;
    
    public function __construct() 
    {
        $this->elements = new ArrayCollection();
    }
    
    public function addFormSubscriber(PurchaseFormSubscriberInterface $element): FormEventSubscriberBuilderInterface 
    {
        if (!$this->elements->contains($element)) {
            $this->elements->add($element);
        }
        
        return $this;
    }

    public function getFormSubscribers(string $className): Collection 
    {
        return $this->elements->filter(function (PurchaseFormSubscriberInterface $element) use ($className) {
            return $element->isSupported($className);
        });
    }

    public function implementSubscriber(FormBuilderInterface $builder, array $options): void 
    {
        $object = $builder->getData();
        if (null === $object) {
            return;
        }
        
        $subscribers = $this->getFormSubscribers(get_class($object));
        
        foreach ($subscribers as $subscriber) {
            $builder->addEventSubscriber($subscriber);
        }
    }

}
