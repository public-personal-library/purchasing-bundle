<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPInterface.php to edit this template
 */

namespace Kematjaya\PurchashingBundle\Builder;

use Symfony\Component\Form\FormBuilderInterface;

/**
 *
 * @author programmer
 */
interface EventSubscriberInjectorInterface 
{
    public function implementSubscriber(FormBuilderInterface $builder, array $options):void;
}
