<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\PurchashingBundle\Event;

use Kematjaya\PurchashingBundle\Entity\PurchaseDetailInterface;
use Kematjaya\ItemPackBundle\Entity\ItemInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Description of PostAddStockEvent
 *
 * @author programmer
 */
class PostAddStockEvent extends Event 
{
    
    /**
     * 
     * @var ItemInterface
     */
    private $item;
    
    /**
     * 
     * @var PurchaseDetailInterface
     */
    private $purchaseDetail;
    
    const EVENT_NAME = 'purchasing.post_add_stock';
    
    public function __construct(ItemInterface $item, PurchaseDetailInterface $purchaseDetail) 
    {
        $this->item = $item;
        $this->purchaseDetail = $purchaseDetail;
    }
    
    public function getItem(): ItemInterface 
    {
        return $this->item;
    }

    public function getPurchaseDetail():PurchaseDetailInterface
    {
        return $this->purchaseDetail;
    }

}
