<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\PurchashingBundle\Event;

use Kematjaya\PurchashingBundle\Entity\PurchaseInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Description of PostSavePurchasingEvent
 *
 * @author programmer
 */
class PostSavePurchasingEvent extends Event 
{
    
    /**
     * 
     * @var PurchaseInterface
     */
    private $purchase;
    
    const EVENT_NAME = 'purchasing.post_insert_purchasing';
    
    public function __construct(PurchaseInterface $purchase) {
        $this->purchase = $purchase;
    }
    
    public function getPurchase(): PurchaseInterface 
    {
        return $this->purchase;
    }


}
