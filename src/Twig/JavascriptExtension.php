<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\PurchashingBundle\Twig;

use Twig\Environment;
use Twig\TwigFunction;
use Twig\Extension\AbstractExtension;

/**
 * Description of JavascriptExtension
 *
 * @author programmer
 */
class JavascriptExtension extends AbstractExtension
{
    
    /**
     * 
     * @var Environment
     */
    private $twig;
    
    public function __construct(Environment $twig) 
    {
        $this->twig = $twig;
    }
    
    public function getFunctions()
    {
        return [
            new TwigFunction('purchase_item_form_javascript',[$this, 'renderJavascript'], ['is_safe' => ['html']])
        ];
    }
    
    public function renderJavascript():string
    {
        return $this->twig->render('@Purchashing/javascript.twig', [
            
        ]);
    }
}
