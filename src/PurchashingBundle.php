<?php

namespace Kematjaya\PurchashingBundle;

use Kematjaya\PurchashingBundle\CompilerPass\FormEventSubscriberCompilerPass;
use Kematjaya\PurchashingBundle\FormSubscriber\PurchaseFormSubscriberInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class PurchashingBundle extends Bundle 
{
    public function build(ContainerBuilder $container) 
    {
        $container->registerForAutoconfiguration(PurchaseFormSubscriberInterface::class)
                ->addTag(PurchaseFormSubscriberInterface::TAG_NAME);
        
        $container->addCompilerPass(new FormEventSubscriberCompilerPass());
        
        parent::build($container);
    }
}
